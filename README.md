#   Dépôt pour les sujets de stage

Vous trouverez dans ce dépôt un [modèle](modèle.md) de sujet de stage
de recherche.

Pour proposer un sujet, créez un fichier à partir du
[modèle](modèle.md) et nommé suivant le format `<votre nom> -
<intitulé abrégé>.md`.
Puis envoyez-le-moi par une Merge Request sur le serveur gitlab ou en
utilisant `git send-email` (si aucune de ces options n’est possible,
en pièce jointe par mail).
